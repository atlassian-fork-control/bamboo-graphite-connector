package com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaManager;
import org.jetbrains.annotations.NotNull;

/**
 * Helper class that handles one concrete value refered by key in BandanaManager.
 *
 * @param <T> Type of the value to be handled.
 */
public class BandanaValue <T> {

    private final BandanaManager bandanaManager;
    private final String key;
    private final T defaultValue;


    /**
     * Constructor
     *
     * @param bandanaManager BandanaManager instance.
     * @param key Key of the value to be handled.
     * @param defaultValue This value will be used if the data is not available in BandanaManager.
     */
    public BandanaValue(@NotNull BandanaManager bandanaManager, String key, T defaultValue ) {
        this.bandanaManager = bandanaManager;
        this.key = key;
        this.defaultValue = defaultValue;
    }


    /**
     * Return the value of the key from BandanaManager
     *
     * If the value is not found in bandana, the default value is used.
     * If the value found in bandana is not acceptable by the type ( ie. isAssignabeFrom
     * returns False ), the default value will be returned, and the bandana value refered by the key will be removed.
     *
     * @return Value
     */
    public T get() {
        T result = defaultValue;
        final Object valueObject = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key );
        if (valueObject == null) {
            return result;
        }
        if (!result.getClass().isAssignableFrom(valueObject.getClass())) {
            // If the object is unassignable, remove it from the bandana manager to prevent any future issues
            bandanaManager.removeValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key );
            throw new RuntimeException("Unassignable object stored in Bandana");

        } else {
            result = (T) valueObject;
        }
        return result;
    }

    /**
     * Set the value of the key in BandanaManager
     *
     * If the value given is null, the default value will be stored in Bandana.
     *
     * @param value
     */

    public void set( T value ){
        if (value == null) {
            value = defaultValue;
        }
        bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key, value);
    }
}
