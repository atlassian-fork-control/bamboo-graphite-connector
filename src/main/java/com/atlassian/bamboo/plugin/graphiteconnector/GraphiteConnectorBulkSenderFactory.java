package com.atlassian.bamboo.plugin.graphiteconnector;

/**
 * This interface provides factory method to get a BulkSender instance.
 */
public interface GraphiteConnectorBulkSenderFactory  {

    /**
     * Returns a BulkSender instance.
     * @return Object supporting the GraphiteConnectorBulkSender interface.
     */
    GraphiteConnectorBulkSender getBulkSender();

}
