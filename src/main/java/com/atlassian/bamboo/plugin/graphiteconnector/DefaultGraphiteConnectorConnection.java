package com.atlassian.bamboo.plugin.graphiteconnector;

import org.jetbrains.annotations.NotNull;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


public class DefaultGraphiteConnectorConnection implements GraphiteConnectorConnection {

    private GraphiteConnectorConfiguration configuration;
    private Logger logger = Logger.getLogger(DefaultGraphiteConnectorConnection.class);

    DefaultGraphiteConnectorConnection(@NotNull GraphiteConnectorConfiguration configuration ) {
        this.configuration = configuration;
    }


    @Override
    public int writeMessageList(@NotNull List<String> metrics) {
        if ( this.configuration.getGraphiteSendingDisabled() ) {
            return 0;
        }
        logger.debug( String.format("Attempting to send %d metrics to graphite server at %s:%s", metrics.size(), configuration.getGraphiteHostName(), configuration.getGraphitePort()) );
        int sentCount = 0;
        try (PrintWriter writer = new PrintWriter(configuration.getSocket().getOutputStream(), true)) {
            for (String metric : metrics ) {
                writer.println(metric);
                ++sentCount;
            }
            writer.flush();
        }
        catch (IOException e) {
            logger.error( String.format( "Unable to send all %d metrics. Error after sending %d metrics, %d remains in the queue." + sentCount, metrics.size(), sentCount, metrics.size() - sentCount ), e);
        }
        logger.debug( String.format("Sent %d metrics to graphite server at %s:%s", metrics.size(), configuration.getGraphiteHostName(), configuration.getGraphitePort()) );
        return sentCount;
    }

}
