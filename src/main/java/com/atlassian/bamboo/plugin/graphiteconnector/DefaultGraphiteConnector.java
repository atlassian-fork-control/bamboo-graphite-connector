package com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.sal.api.ApplicationProperties;
import org.jetbrains.annotations.NotNull;


/**
 * GraphiteConnector Implementation
 *
 * All messages will be sent to graphite instantly when write() is called. This means that
 * each write() call will result a socket() open and close. If you want to send multiple
 * messages please use getBulkSender() to get a GraphiteConnectorBulkSender instance, which
 * allows queueing up messages, and sending them over one connection.
 *
 */
public class DefaultGraphiteConnector implements GraphiteConnector {

    private final GraphiteConnectorConfiguration configuration;
    private final GraphiteConnectorConnection connection;


    public DefaultGraphiteConnector(@NotNull GraphiteConnectorConfiguration configuration) {
        this.configuration = configuration;
        this.connection = new DefaultGraphiteConnectorConnection( configuration );
    }

    @Override
    public GraphiteConnectorConfiguration getConfiguration() {
        return this.configuration;
    }


    @Override
    public GraphiteConnectorBulkSender getBulkSender() {
        return new DefaultGraphiteConnectorBulkSender( configuration, connection );
    }

    @Override
    public void write(String path, String label, String value) {
        this.write( path, label, value, System.currentTimeMillis() / 1000L );
    }

    @Override
    public void write(String path, String label, String value, long timestamp) {
        GraphiteConnectorBulkSender sender = this.getBulkSender();
        sender.write( path, label, value, timestamp );
        sender.send();
    }
}